﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScript : MonoBehaviour
{
    public string sceneName;

    private void OnMouseDown()
    {
        //シーンを切り替える
        SceneManager.LoadScene(sceneName);
    }
}
