﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gholdamage : MonoBehaviour
{
    public float hp = 2;
    public bool on_damage = false;
    private SpriteRenderer renderer;
    public damage Player;
    public GameObject DeathAnime;

    // Use this for initialization
    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (on_damage)
        {
            //Abs正負変換、sin円周
            float level = Mathf.Abs(Mathf.Sin(Time.time * 5));
            //赤青緑透明度
            renderer.color = new Color(1f, 1f, 1f, level);
        }
    }

    void OnDamageEffect(float damage)
    {
        if (!on_damage)
        {
            // 非ダメージ時
            hp = hp - damage;

            if (hp <= 0)
            {
                //死亡処理          
                Instantiate(DeathAnime, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            else
            {
                //無敵
                on_damage = true;
                StartCoroutine("EnemyDamage");
            }
        }
    }

    IEnumerator EnemyDamage()
    {
        yield return new WaitForSeconds(0.1f);

        on_damage = false;
        renderer.color = new Color(1f, 1f, 1f, 1f);
    }

    // 炎にぶつかった際の処理
    void OnTriggerEnter2D(Collider2D col)
    {

        //  炎にぶつかったかつダメージフラグがfalse
        if (col.gameObject.tag == "PlayerAttack")
        {
            float damage = Player.GetDamage();
            OnDamageEffect(damage);
        }
    }
}
