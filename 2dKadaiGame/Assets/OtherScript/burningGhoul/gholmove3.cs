﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gholmove3 : MonoBehaviour
{
    //変数定義
    private Rigidbody2D rb;
    private Vector2 defaultpass;
    private float timeleft;
    private bool flip = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        defaultpass = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //X座標のみ横移動
        rb.MovePosition(new Vector2(defaultpass.x - Mathf.PingPong(Time.time * 6, 12), 0));

        flip = this.GetComponent<SpriteRenderer>().flipX;
        //だいたい1秒ごとに処理を行う
        timeleft -= Time.deltaTime;
        if (timeleft <= 0.0)
        {
            timeleft = 2f;

            if (this.GetComponent<SpriteRenderer>().flipX)
            {
                //左右に向きを変える
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
    }
}
