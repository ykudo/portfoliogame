﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyDeath : MonoBehaviour
{
    public AudioClip deathSE;

    public void enemyDeathAnim()
    {
        // オーディオを再生
        AudioSource.PlayClipAtPoint(deathSE, transform.position);
        Destroy(this.gameObject);
    }
}
