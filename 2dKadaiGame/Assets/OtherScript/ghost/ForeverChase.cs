﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForeverChase : MonoBehaviour
{
    GameObject targetObject;
    Rigidbody2D rbody;
    public string targetObjectName;
    public float speed = 1;

    // Start is called before the first frame update
    void Start()
    {
        //目標オブジェクトを見つけておく
        targetObject = GameObject.Find(targetObjectName);

        //重力を0にして、衝突時に回転させない。
        rbody = GetComponent<Rigidbody2D>();
        rbody.gravityScale = 0;
        rbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private void FixedUpdate()
    {
        Vector3 dir = (targetObject.transform.position - this.transform.position).normalized;
        float vx = dir.x * speed;
        float vy = dir.y * speed;
        rbody.velocity = new Vector2(vx, vy);
        //移動の向きで左右に向きを変える
        this.GetComponent<SpriteRenderer>().flipX = (vx < 0);
    }

}
