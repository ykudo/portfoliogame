﻿using UnityEngine;
using System.Collections;

public class ghostDamage : MonoBehaviour
{
    public float hp = 3;
    public bool on_damage = false;
    private SpriteRenderer renderer;
    public damage Player;
    public GameObject DeathAnime;

    // Use this for initialization
    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (on_damage)
        {
            float level = Mathf.Abs(Mathf.Sin(Time.time * 10));
            renderer.color = new Color(1f, 1f, 1f, level);
        }
    }

    void OnDamageEffect(float damage)
    {
        if (!on_damage)
        {
            // gotDamage
            hp = hp - damage;

            if (hp <= 0)
            {
                Instantiate(DeathAnime, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            else
            {
                //changeFlg
                on_damage = true;
                StartCoroutine("EnemyDamage");
            }
        }
    }

    IEnumerator EnemyDamage()
    {
        yield return new WaitForSeconds(0.2f);

        on_damage = false;
        renderer.color = new Color(1f, 1f, 1f, 1f);
    }

    // 炎にぶつかった際の処理
    void OnTriggerEnter2D(Collider2D col)
    {

        //  炎にぶつかったかつダメージフラグがfalse
        if (col.gameObject.tag == "PlayerAttack")
        {
            float damage = Player.GetDamage();
            OnDamageEffect(damage);
        }
    }
}
