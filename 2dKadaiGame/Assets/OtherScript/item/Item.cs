﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    // アイテム取得音
    public AudioClip ItemSE;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            //取得音
            AudioSource.PlayClipAtPoint(ItemSE, transform.position);
            //消去
            Destroy(this.gameObject);
        }
    }
}
