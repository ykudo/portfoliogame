﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skeltonMove : MonoBehaviour
{
    public bool flipFlag = false;
    Rigidbody2D rb;
    private SpriteRenderer renderer;     //画像編集
    private bool move = false;
    private Vector2 defaultpass;
    private float seconds;
    private bool flip;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        defaultpass = transform.position;
        StartCoroutine("EnemyRise");
        flip = this.GetComponent<SpriteRenderer>().flipX;
    }

    void FixedUpdate()
    {
        if (move)
        {
            if (!flip)
            {
                //X座標のみ横移動（左歩き）
                rb.velocity = new Vector2(-0.5f, 0);
            }
            else
            {
                //X座標のみ横移動(右歩き）
                rb.velocity = new Vector2(+0.5f, 0);
            }
                    
        }
    }

    // 登場アニメ中は停止
    IEnumerator EnemyRise()
    {
        yield return new WaitForSeconds(3);
        move = true;

    }
}
