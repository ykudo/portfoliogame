﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skeltonMove2 : MonoBehaviour
{
    GameObject targetObject;
    Rigidbody2D rbody;
    public string targetObjectName;
    public float speed = 1;
    bool move = false;

    //アニメ
    public Animator Skelton;

    // Start is called before the first frame update
    void Start()
    {
        //目標オブジェクトを見つけておく
        targetObject = GameObject.Find(targetObjectName);

        //重力を0にして、衝突時に回転させない。
        rbody = GetComponent<Rigidbody2D>();
        rbody.gravityScale = 0;
        rbody.constraints = RigidbodyConstraints2D.FreezeRotation;

        //登場アニメ時は移動しない
        var openCoroutine = StartCoroutine("WaitAnimationEnd", "Entry");
    }

    private void FixedUpdate()
    {
        if (move)
        {
            Vector3 dir = (targetObject.transform.position - this.transform.position).normalized;
            float vx = dir.x * speed;
            rbody.velocity = new Vector2(vx, 0);
            //移動の向きで左右に向きを変える
            this.GetComponent<SpriteRenderer>().flipX = (vx > 0);
        }
    }

    //アニメーション終了を判定するコルーチン
    private IEnumerator WaitAnimationEnd(string entry)
    {
        yield return new WaitForSeconds(3);
        move = true;
    }

}
