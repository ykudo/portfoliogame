﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyTrap : MonoBehaviour
{
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {

            enemy1.SetActive(true);
            enemy2.SetActive(true);
            enemy3.SetActive(false);
            Destroy(this.gameObject);
        }
    }
}
