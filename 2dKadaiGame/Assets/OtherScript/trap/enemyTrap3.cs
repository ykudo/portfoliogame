﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyTrap3 : MonoBehaviour
{
    public GameObject enemy1;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {

            enemy1.SetActive(true);
                
            Destroy(this.gameObject);
        }
    }
}
