﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyTrap6 : MonoBehaviour
{
    public GameObject enemy1;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {

            Destroy(enemy1);

            Destroy(this.gameObject);
        }
    }
}
