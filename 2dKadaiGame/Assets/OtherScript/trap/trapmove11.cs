﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapmove11 : MonoBehaviour
{
    //変数定義
    private Rigidbody2D rb;
    private Vector2 defaultpass;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        defaultpass = transform.position;
    }

    void Update()
    {
        //X座標のみ横移動
        rb.velocity = new Vector2(1.5f, 0);
    }
}
