﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapmove14 : MonoBehaviour
{
    //変数定義
    private Rigidbody2D rb;
    private Vector2 defaultpass;

    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        defaultpass = transform.position;
    }

    void Update()
    {
        //Y座標のみ縦移動
        rb.MovePosition(new Vector2(defaultpass.x, defaultpass.y + Mathf.PingPong(Time.time * 2, 6)));
    }
}
