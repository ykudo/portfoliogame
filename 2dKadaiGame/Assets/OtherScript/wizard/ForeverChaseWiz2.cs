﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForeverChaseWiz2 : MonoBehaviour
{
    GameObject targetObject;
    Rigidbody2D rbody;
    public string targetObjectName;
    // Ememyballプレハブ
    public GameObject ball;

    //発射位置
    public Transform muzzle;

    //アニメ
    public Animator Wizard;

    // Start is called before the first frame update
    void Start()
    {
        //目標オブジェクトを見つけておく
        targetObject = GameObject.Find(targetObjectName);

        //重力を0にして、衝突時に回転させない。
        rbody = GetComponent<Rigidbody2D>();
        rbody.gravityScale = 3;
        rbody.constraints = RigidbodyConstraints2D.FreezeRotation;

        //0秒後にCall関数を実行し、その後は2秒間隔で実行し続ける
        InvokeRepeating("Attack", 0, 2);
    }

    private void FixedUpdate()
    {
        Vector3 dir = (targetObject.transform.position - this.transform.position).normalized;
        float vx = dir.x;
        //移動の向きで左右に向きを変える
        this.GetComponent<SpriteRenderer>().flipX = (vx > 0);
    }

    private void Attack()
    {
        GetComponent<Animator>().SetBool("Attack", true);
        // 弾をプレイヤーと同じ位置/角度で作成 
        var openCoroutine = StartCoroutine("WaitAnimationEnd", "Attack");
    }

    //アニメーション終了を判定するコルーチン
    private IEnumerator WaitAnimationEnd(string attack)
    {
        bool finish = false;
        while (!finish)
        {
            AnimatorStateInfo nowState = Wizard.GetCurrentAnimatorStateInfo(0);
            if (nowState.IsName(attack))
            {
                yield return new WaitForSeconds(0.5f);
                GameObject balls = Instantiate(ball, transform.position, transform.rotation);
                balls.transform.position = muzzle.position;
                GetComponent<Animator>().SetBool("Attack", false);
                finish = true;
            }
            else
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}


