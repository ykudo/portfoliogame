﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wizardAttack : MonoBehaviour
{
    public int speed = 3;
    GameObject Ball;
    GameObject Enemy;
    private bool flip;
    //弾の寿命
    public float life_time = 2f;
    float time = 0f;

    void Start()
    {
        Enemy = GameObject.FindGameObjectWithTag("wizard");     
        Ball = transform.FindChild("ball").gameObject;
        flip = Enemy.GetComponent<SpriteRenderer>().flipX;
        //右向きがtrue
        if (flip)
        {
            GetComponent<Rigidbody2D>().velocity = transform.right * speed;
        }
        else
        {
            //弾の画像を左向きに変える
            Ball.GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<Rigidbody2D>().velocity = transform.right * -1 * speed;
        }
    }

    private void Update()
    {
        time += Time.deltaTime;
        if (time > life_time)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "PlayerAttack")
        {
            Destroy(this.gameObject);
        }
    }
}
