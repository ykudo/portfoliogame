﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour
{
    public int speed = 10;
    GameObject Bullet;
    GameObject Player;
    private bool flip;
    public AudioClip hitSE;
    AudioSource audioSource;

    //弾の寿命
    public float life_time = 1.2f;
    float time = 0f;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Bullet = transform.FindChild("Bullet").gameObject;
        flip = Player.GetComponent<SpriteRenderer>().flipX;
        audioSource = GetComponent<AudioSource>();
        //右向きがtrue
        if (flip)
        {
            GetComponent<Rigidbody2D>().velocity = transform.right * speed;
        }
        else
        {
            //弾の画像を左向きに変える
            Bullet.GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<Rigidbody2D>().velocity = transform.right * -1 * speed;
        }
        time = 0;
    }

    void Update()
    {
        time += Time.deltaTime;
        if (time > life_time)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "enemy" || col.gameObject.tag == "wizard" || col.gameObject.tag == "ghost")
        {
            //ヒットSE
            AudioSource.PlayClipAtPoint(hitSE, transform.position);
            Destroy(this.gameObject);
        }
    }
}
