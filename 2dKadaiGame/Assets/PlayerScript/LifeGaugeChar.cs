﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LifeGaugeChar : MonoBehaviour
{
    //　HP
    [SerializeField]
    private int hp;
    //　LifeGaugeスクリプト
    [SerializeField]
    private LifeGauge lifeGauge;
    private int maxHp;
    public string sceneName1;


    // Start is called before the first frame update
    void Start()
    {
        //　体力の初期化
        hp = 3;
        maxHp = 3;
        //　体力ゲージに反映
        lifeGauge.SetLifeGauge(hp);
    }

    //　ダメージ処理メソッド（全削除＆HP分作成）
    public void Damage(int damage)
    {
        hp -= damage;
        //　0より下の数値にならないようにする
        hp = Mathf.Max(0, hp);

        if (hp >= 0)
        {
            lifeGauge.SetLifeGauge(hp);
        }
    }
    //　ダメージ処理メソッド（ダメージ数分だけアイコンを削除）
    public void onDamage(int damage)
    {
        hp -= damage;
        if (hp < 0)
        {
            //　ダメージ調整
            damage = Mathf.Abs(hp + damage);
            hp = 0;
        }
        if (hp == 0)
        {
            GetComponent<Animator>().SetBool("Death", true);
            // コルーチン開始
            StartCoroutine("Death");

        }
        if (damage > 0)
        {
            lifeGauge.SetLifeGaugeDamage(damage);
        }
    }

    //回復処理メソッド（回復分だけアイコン増加）
    //体力最大値以上は回復不可
    public void onHeal(int heal)
    {
        if (hp != maxHp)
        {
            int currentHp = hp;
            hp += heal;

            //最大値以上ならリセット
            if (hp > maxHp)
            {
                hp = maxHp;
                lifeGauge.SetLifeGaugeHeal(maxHp - currentHp);
            }
            else
            {
                lifeGauge.SetLifeGaugeHeal(heal);
            }

        }
    }

    //体力最大値アップ + 全回復
    public void onLifeMaxUp()
    {
        int currentHp = hp;
        maxHp += 1;
        Debug.Log(hp);
        hp = maxHp;
        lifeGauge.SetLifeGaugeHeal(maxHp - currentHp);
    }

    IEnumerator Death()
    {
        // 3秒間処理を止めてゲームオーバー
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(sceneName1);

    }
}
