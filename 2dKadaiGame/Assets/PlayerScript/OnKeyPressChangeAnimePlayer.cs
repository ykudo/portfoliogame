﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnKeyPressChangeAnimePlayer : MonoBehaviour
{
    private bool jumpFlag = false;
    private bool leftFlag = true;
    private bool jumpAttackFlg = true;
    private bool jimenFlag = true;

    public float jumpPower = 6;
    public float speed = 4;
  
    public AudioClip attackSE;
    AudioSource audioSource;

    Rigidbody2D rbody;
    Animator animator;
   
    // PlayerBulletプレハブ
    public GameObject bullet;
    public GameObject sword;

    //発射位置
    public Transform muzzle;

    float vx = 0;
    // Start is called before the first frame update
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        rbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //動作
        vx = 0;

        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("LightGuard_Death"))
        {
            //アニメ動作中は移動不可、
            if (Input.GetKey(KeyCode.D) && jimenFlag && !animator.GetCurrentAnimatorStateInfo(0).IsName("LightGuard_Attack") &&
                !animator.GetCurrentAnimatorStateInfo(0).IsName("LightGuard_Hurt") &&
                !animator.IsInTransition(0) || Input.GetKey(KeyCode.D) && !jimenFlag)
            {
                vx = speed;
                leftFlag = true;
            }
            if (Input.GetKey(KeyCode.A) && jimenFlag && !animator.GetCurrentAnimatorStateInfo(0).IsName("LightGuard_Attack") &&
                !animator.GetCurrentAnimatorStateInfo(0).IsName("LightGuard_Hurt") &&
                !animator.IsInTransition(0) || Input.GetKey(KeyCode.A) && !jimenFlag)
            {
                vx = -speed;
                leftFlag = false;
            }

            //走りアニメ
            if (Input.GetKey(KeyCode.A))
            {
                GetComponent<Animator>().SetInteger("AnimState", 2);
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                GetComponent<Animator>().SetInteger("AnimState", 3);
            }

            if (Input.GetKey(KeyCode.D))
            {
                GetComponent<Animator>().SetInteger("AnimState", 2);
            }

            if (Input.GetKeyUp(KeyCode.D))
            {
                GetComponent<Animator>().SetInteger("AnimState", 3);
            }

            //攻撃アニメ
            //攻撃アニメーション中でなく、他のアニメーション遷移中でない場合攻撃可
            if (Input.GetKeyDown(KeyCode.W) && !animator.GetCurrentAnimatorStateInfo(0).IsName("LightGuard_Attack") &&
                !animator.IsInTransition(0) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A) && !jumpFlag)
            {
                // ショット音を鳴らす
                GetComponent<AudioSource>().Play();
                GetComponent<Animator>().SetBool("Attack", true);
                // 弾をプレイヤーと同じ位置/角度で作成
                GameObject bullets = Instantiate(sword, transform.position, transform.rotation);
                bullets.transform.position = muzzle.position;

            }
            else
            {
                GetComponent<Animator>().SetBool("Attack", false);
            }

            //ジャンプ攻撃
            //一回のみ
            if(Input.GetKeyDown(KeyCode.W) && jumpFlag && jumpAttackFlg)
            {
                AudioSource.PlayClipAtPoint(attackSE, transform.position);
                // 弾をプレイヤーと同じ位置/角度で作成
                GameObject bullets = Instantiate(bullet, transform.position, transform.rotation);
                bullets.transform.position = muzzle.position;
                jumpAttackFlg = false;
            }

            //スペースキーが押されたらジャンプ
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //ジャンプするとき
                if (jimenFlag)
                {
                    jumpFlag = true;
                    rbody.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse);
                }
            }

            //着地判定
            if (Mathf.Abs(rbody.velocity.y) < 0.001f)
            {
                jumpFlag = false;
                jumpAttackFlg = true;
                GetComponent<Animator>().SetBool("Jump", false);
            }
            else
            {
                GetComponent<Animator>().SetBool("Jump", true);
            }
        }

        //重力をかける&移動
        rbody.velocity = new Vector2(vx, rbody.velocity.y);
        this.GetComponent<SpriteRenderer>().flipX = leftFlag;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "PlayerAttack")
        {   
            jimenFlag = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "PlayerAttack")
        {
            jimenFlag = false;
        }
    }
}