﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnKeyPressMoveSprite : MonoBehaviour
{
    public float speed = 3;
    float vx = 0;
    bool leftFlag = true;
    Rigidbody2D rbody;
    // Start is called before the first frame update
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        rbody.gravityScale = 1;
        rbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    // Update is called once per frame
    void Update()
    {
        vx = 0;
        if (Input.GetKey(KeyCode.D))
        {
            vx = speed;
            leftFlag = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            vx = -speed;
            leftFlag = false;
        }
    }

    private void FixedUpdate()
    {
        this.transform.Translate(vx / 50, 0, 0);
        this.GetComponent<SpriteRenderer>().flipX = leftFlag;
    }


}
