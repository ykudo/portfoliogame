﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damage : MonoBehaviour
{
    public GameObject player;
    public bool on_damage = false;
    public bool on_heal = false;
    public AudioClip damageSE;

    private SpriteRenderer renderer;

    AudioSource audioSource;

    //敵に与えるダメージ
    public float eneDamage = 1;

    // Start is called before the first frame update
    void Start()
    {
        //点滅処理の為に呼び出しておく
        renderer = gameObject.GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // ダメージフラグがtrueで有れば点滅させる
        if (on_damage)
        {
            float level = Mathf.Abs(Mathf.Sin(Time.time * 10));
            renderer.color = new Color(1f, 1f, 1f, level);
        }
    }

    // 敵の接触処理1
    void OnCollisionEnter2D(Collision2D col)
    {

        //  敵とぶつかったかつダメージフラグがfalse
        if (!on_damage && col.gameObject.tag == "enemy")
        {
            

            player.gameObject.SendMessage("onDamage", 1);
            OnDamageEffect();
        }

    }

    //接触処理
    void OnTriggerEnter2D(Collider2D col)
    {
        //敵の接触処理
        if(!on_damage && col.gameObject.tag == "wizard" || 
            !on_damage && col.gameObject.tag == "ghost" || 
            !on_damage && col.gameObject.tag == "enemy" ||
            !on_damage && col.gameObject.tag == "trap")
        {
            //ダメージ音
            AudioSource.PlayClipAtPoint(damageSE, transform.position);
            player.gameObject.SendMessage("onDamage", 1);
            OnDamageEffect();
        }
        if (!on_damage && col.gameObject.tag == "trap2")
        {
            //ダメージ音
            AudioSource.PlayClipAtPoint(damageSE, transform.position);
            player.gameObject.SendMessage("onDamage", 7);
            OnDamageEffect();
        }

        //回復アイテムの接触処理
        if(!on_heal && col.gameObject.tag == "Life" ||
            !on_heal && col.gameObject.tag == "superLife" ||
            !on_heal && col.gameObject.tag == "lifeMaxUp")
        {
            on_heal = true;
            if (col.gameObject.tag == "Life") {
                player.gameObject.SendMessage("onHeal", 1);
            }
            else
            {
                player.gameObject.SendMessage("onHeal", 7);
            }

            if (col.gameObject.tag == "lifeMaxUp")
            {
                player.gameObject.SendMessage("onLifeMaxUp");
            }
                StartCoroutine("Heal");
        }

        //攻撃力アップアイテムの接触処理
        if (col.gameObject.tag == "attackUp")
        {
            eneDamage += 1f;
        }
    }

        //　ダメージを受けた際の動き
        void OnDamageEffect()
    {
        // ダメージフラグON
        on_damage = true;

        // プレイヤーの位置を後ろに飛ばす
        GetComponent<Animator>().SetBool("Hurt", true);

        // プレイヤーのflipXでどちらを向いているのかを判定
        if (this.GetComponent<SpriteRenderer>().flipX)
        {
            transform.Translate(new Vector3(-2, 0.6f, 0));
        }
        else
        {
            transform.Translate(new Vector3(2, 0.6f, 0));
        }

        // コルーチン開始
        StartCoroutine("Damage");
    }

    IEnumerator Damage()
    {
        // 0.1秒間処理を止めてダメージアニメ再生
        yield return new WaitForSeconds(0.1f);
        GetComponent<Animator>().SetBool("Hurt", false);

        // 0.6秒後ダメージフラグをfalseにして点滅を戻す(無敵解除)
        yield return new WaitForSeconds(0.6f);
        on_damage = false;
        renderer.color = new Color(1f, 1f, 1f, 1f);
    }

    //何故か回復処理が一度に二回発生するので一瞬処理停止
    IEnumerator Heal()
    {
        // 0.1秒間処理を止める
        yield return new WaitForSeconds(0.1f);
        on_heal = false;
    }

    public float GetDamage()
    {
        return eneDamage;
    }
}
