﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageData : MonoBehaviour
{
    public string Name { get; set; }
    public int ClearTime { get; set; }
    //public string Date { get; set; }

    public MessageData()
    {
        Name = "";
        ClearTime = 0;
        //Date = "";
    }
}
