﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameClear : MonoBehaviour
{
    [SerializeField] private GameObject timerGameObject;

    public string sceneName1;
    private static Text clearTime;
    private static float clearTimeFlo;


    //シーン切り替え
    void OnTriggerEnter2D(Collider2D col)
    {
        Timer timer;
        //timer = タイマークラスのアタッチされているゲームオブジェクト.GetComponent<Timer>();
        timer = timerGameObject.GetComponent<Timer>();
        
        clearTime = timer.GetClearTime();
        clearTimeFlo = timer.GetClearTimeFlo();
        Debug.Log(clearTimeFlo);

        SceneManager.LoadScene(sceneName1);
        Destroy(this.gameObject);
    }

    public static Text GetClearTime()
    {
        return clearTime;
    }
    public static float GetClearTimeFlo()
    {
        return clearTimeFlo;
    }
}
