﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStart2 : MonoBehaviour
{
    public string sceneName1;

    void Update()
    {
        //シーン切り替え
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneName1);
        }
    }
}
