﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MyGameApi : MonoBehaviour
{
    [SerializeField] private Text rankingName1;
    [SerializeField] private Text rankingName2;
    [SerializeField] private Text rankingName3;

    [SerializeField] private Text rankingTime1;
    [SerializeField] private Text rankingTime2;
    [SerializeField] private Text rankingTime3;

    public void OnClickGetButton()
    {
        GetJsonFromWebRequest();
    }

    public void OnClickSetButton()
    {
        //SetJsonFromWebRequest();
    }

    public void GetJsonFromWebRequest()
    {
        const string url = "http://localhost/SplatterRoomApi/clearranking/getRanking";

        StartCoroutine(GetRanking(url, CallBackSuccess, CallBackFailed));
    }

    public void SetJsonFromWebRequest(String name,int clearTime)
    {
        const string url = "http://localhost/SplatterRoomApi/clearranking/setRanking";

        StartCoroutine(SetRanking(url, name, clearTime, CallBackSuccess, CallBackFailed));
    }

    private void CallBackSuccess(string text)
    {
        Debug.Log(text);
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        List<MessageData> messageList = MessageDataModel.DeserializeFromJson(text);

        string temp = "";

        foreach (var message in messageList)
        {
            temp += message.Name + message.ClearTime.ToString() + "\n" ;
        }

        Debug.Log(temp);
        
        /*
        String[] sStrOutputName = new String[messageList.];
        int[] sStrOutputTime = new int[3];
        for (int i = 0; i < 3; i++)
        {
            sStrOutputName[i] = messageList[i].Name;
            sStrOutputTime[i] = messageList[i].ClearTime;

        }
 
        rankingName1.text = sStrOutputName[0];
        rankingName2.text = sStrOutputName[1];
        rankingName3.text = sStrOutputName[2];

        rankingTime1.text = sStrOutputTime[0].ToString();
        rankingTime2.text = sStrOutputTime[1].ToString();
        rankingTime3.text = sStrOutputTime[2].ToString();
        */
    }

    private void CallBackFailed()
    {
        Debug.Log("Failed");
    }

    private IEnumerator GetRanking(string url, Action<string> callBackSuccess, Action callBackFailed = null)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(url);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (callBackFailed != null)
            {
                callBackFailed();
            }
        }
        else if (webRequest.isDone)
        {
            if (callBackSuccess != null)
            {
                callBackSuccess(webRequest.downloadHandler.text);
            }
        }


    }

    private IEnumerator SetRanking(string url, String name,int clearTime,Action<string> callBackSuccess, Action callBackFailed = null)
    {
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("name", name);
        wwwForm.AddField("clearTime", clearTime);

        UnityWebRequest webRequest = UnityWebRequest.Post(url, wwwForm);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (callBackFailed != null)
            {
                callBackFailed();
            }
        }
        else if (webRequest.isDone)
        {
            if (callBackSuccess != null)
            {
                callBackSuccess(webRequest.downloadHandler.text);
            }
        }
    }
}

