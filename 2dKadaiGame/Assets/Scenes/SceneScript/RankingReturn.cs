﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//MiniJson
using MiniJSON;
using System.Collections.Generic;


public class RankingReturn : MonoBehaviour
{
    public string sceneName1;
    [SerializeField] private GameObject rankingObject;

    private void Start()
    {
        MyGameApi ranking = rankingObject.GetComponent<MyGameApi>();
        //ranking.GetJsonFromWebRequest();
        ranking.SetJsonFromWebRequest("name",50);

    }
    void Update()
    {

        //シーン切り替え
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneName1);
        }
    }
}
