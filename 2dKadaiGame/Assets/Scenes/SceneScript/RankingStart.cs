﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RankingStart : MonoBehaviour
{
    public string sceneName;


    private void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        //シーン切り替え
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
