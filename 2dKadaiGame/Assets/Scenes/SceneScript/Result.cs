﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Result : MonoBehaviour
{
    [SerializeField] private GameObject timerGameObject;
    public string sceneName1;
    private Text clearTime;
    public Text clearTimeDisp;
    private float clearTimeFlo;
    public Text clearTimeRank;

    private void Start()
    {
        clearTime = GetComponent<Text>();
        clearTimeDisp = timerGameObject.GetComponent<Text>();

        //クリアタイム
        clearTime = GameClear.GetClearTime();
        clearTimeDisp.text = clearTime.text;

        //クリアタイム評価
        clearTimeFlo = GameClear.GetClearTimeFlo();
        if(clearTimeFlo <= 180)
        {
            clearTimeRank.text = "Excellent";
        }
        else if(clearTimeFlo <= 240)
        {
            clearTimeRank.text = "Good";
        }
        else
        {
            clearTimeRank.text = "Average";
        }

    }

    void Update()
    {

        //シーン切り替え
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneName1);
        }
    }
}
